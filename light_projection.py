from tkinter import *
from projection import transform_image
from tkinter import filedialog
from tkinter import messagebox
import xml.etree.cElementTree as et
from PIL import ImageTk
from tkinter import font


def is_svg(filename):
    tag = None
    with open(filename, "r") as f:
        try:
            for event, el in et.iterparse(f, ('start',)):
                tag = el.tag
                break
        except et.ParseError:
            pass
    return tag == '{http://www.w3.org/2000/svg}svg'


class Window(Canvas):

    def __init__(self, master=None):
        Canvas.__init__(self, master)
        self.font = font.Font(family="Helvetica", size=12)
        self.bold_font = font.Font(family="Helvetica", size=12, weight="bold")
        self.master = master
        self.photo = ImageTk.PhotoImage(file='projection_image.jpeg')
        self.panel = Label(self, image=self.photo)
        self.panel.pack(side="bottom")
        self.panel.place(x=0, y=350)
        self.init_window()

    def browsecsv(self):
        self.new_label = None
        Tk().withdraw()
        self.filename = filedialog.askopenfilename()
        self.status_label.config(text='Файл %s отримано' % (self.filename.split('/')[-1]))

    def validate_float(self, value, default_value):
        try:
            val = float(value)
        except (TypeError, ValueError):
            val = default_value
            messagebox.showwarning("Warning", "Неможливо зробити проекцію з заданим значенням %s. "
                                              "Перехід до початкових налаштувань" % value)
        return val

    def validate_svg_file(self, value):
        if value is None:
            messagebox.showerror('Файл відсутній', 'Ви не визначили файл')
            return None
        try:
            f = open(value)
            f.close()
            if is_svg(value):
                return value
            else:
                messagebox.showerror('Помилка', 'Це не svg файл. Будь ласка, спробуйте іінший файл')
        except (FileNotFoundError, TypeError):
            messagebox.showerror('Не файл', 'Те, що вставлено - не файл')
        return None

    def process(self):
        fin = self.validate_svg_file(self.filename)
        if not fin:
            return None
        size_x = self.validate_float(self.size_x.get(1.0, END), 600)
        size_y = self.validate_float(self.size_y.get(1.0, END), 600)
        height = self.validate_float(self.height.get(1.0, END), 100)
        shadow_to_template = self.validate_float(self.shadow_to_template.get(1.0, END), 150)
        dist = self.validate_float(self.template_to_light.get(1.0, END), 100)
        transform_image(fin, size_x=size_x, size_y=size_y, height=height, shadow_to_template=shadow_to_template,
                        template_to_light=dist)
        self.status_label.config(text='Готово!')

    def init_window(self):
        self.master.title("Проекція")
        self.new_label = None
        self.status_label = Label(self, text='Будь ласка, вставте файл', font=self.font)
        self.browse_button = Button(self, text="Вибрати зображення", command=self.browsecsv)
        self.size_x = Text(self, width=20, height=1)
        self.size_y = Text(self, width=20, height=1)
        self.height = Text(self, width=20, height=1)
        self.shadow_to_template = Text(self, width=20, height=1)
        self.filename = None
        self.template_to_light = Text(self, width=20, height=1)
        self.transform_button = Button(self, text="Трансформувати", command=self.process)
        self.x_label = Label(self, text='Ширина(W2)', font=self.font)
        self.y_label = Label(self, text='Висота(W1)', font=self.font)
        self.height_label = Label(self, text='Висота точки світла(H1)', font=self.font)
        self.shadow_to_template_label = Label(self, text='Від тіні до маски(L2)', font=self.font)
        self.dist_label = Label(self, text='Від світла до маски(L1)', font=self.font)
        self.shadow_label = Label(self, text='Розміри тіні, мм', font=self.bold_font)
        self.distance_label = Label(self,  text='Відстані, мм', font=self.bold_font)
        self.shadow_label.place(x=10, y=10)
        self.distance_label.place(x=10, y=110)
        self.status_label.place(x=10, y=300)
        self.size_x.place(x=250, y=40)
        self.x_label.place(x=50, y=40)
        self.size_y.place(x=250, y=70)
        self.y_label.place(x=50, y=70)
        self.height.place(x=250, y=140)
        self.height_label.place(x=50, y=140)
        self.shadow_to_template.place(x=250, y=170)
        self.shadow_to_template_label.place(x=50, y=170)
        self.template_to_light.place(x=250, y=200)
        self.dist_label.place(x=50, y=200)
        self.browse_button.place(x=50, y=240)
        self.create_line(0, 100, 500, 100, fill='grey')
        self.create_line(0, 230, 500, 230, fill='grey')
        self.transform_button.place(x=350, y=240)
        self.pack(fill=BOTH, expand=1)

root = Tk()
root.geometry("650x750")
app = Window(root)

root.mainloop()
