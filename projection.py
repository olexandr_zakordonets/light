from lxml import etree
import re
import os
import math
from svgpathtools import svg2paths, wsvg
TAU = math.pi*2


class Point(object):
    def __init__(self, x, y, z=0):
        self.x = x
        self.y = y
        self.z = z


def transfer_point(x, y, orig_y, destination, min_x=None, max_y=None):
    """
    All the Magic is performed here
    """
    xAu = destination.x
    yAu = destination.y
    zAu = destination.z
    z_new = zAu*(y+orig_y)/(yAu+y)
    x_new = x+z_new/zAu*(xAu-x)
    if min_x is not None and max_y is not None:
        return Point(float(x_new)-min_x, max_y-float(z_new))
    else:
        return Point(float(x_new), float(z_new))


def string_to_list(string):
    patt1 =r'[mzlhvcsqta]|-?[0-9.]+'
    path_arr = re.findall(patt1, string, flags=re.IGNORECASE)
    new_pattern = r'[mzlhvcsqta]'
    for i in range(len(path_arr)):
        if not re.search(new_pattern, path_arr[i], flags=re.IGNORECASE):
            path_arr[i] = float(path_arr[i])
    return path_arr


def list_to_string(path_list):
    path_str = ''
    for i in range(len(path_list)):
        if type(path_list[i-1]) is int and type(path_list[i]) is int:
            path_str += ' ' + str(path_list[i])
        else:
            path_str += ' '+ str(path_list[i])
    return path_str


def distort_path(path_str, orig_y, projection_center, max_x=None, max_y=None, coef_x=1, coef_y=1):
    path_arr = string_to_list(path_str)
    path_arr2 = []
    xy_counter = 0
    prev = 0
    for element in path_arr:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            subpath_type = str(element)
            is_num = False
            path_arr2.append(element)
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy = "x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                point = transfer_point(float(prev), element, orig_y, projection_center, max_x, max_y)
                path_arr2.append(point.x*coef_x)
                path_arr2.append(point.y*coef_y)
        xy_counter += 1
    path_str = list_to_string(path_arr2)
    return path_str


def get_vb_path(vb):
    min_x, min_y, max_x, max_y = (float(i) for i in vb.split())
    max_x += min_x
    max_y += min_y
    return ' '.join([str(min_x), str(min_y), str(max_x), str(min_y), str(min_x), str(max_y), str(max_x), str(max_y)])


def retrieve_vb_from_rect(rect):
    rect_coords = [float(i) for i in rect.split()]
    x_coords = [rect_coords[i] for i in range(len(rect_coords)) if i % 2 == 0]
    y_coords = [rect_coords[i] for i in range(len(rect_coords)) if i % 2 == 1]
    return min(x_coords), max(x_coords), min(y_coords),  max(y_coords)


def get_min_x(path_str):
    path_arr = string_to_list(path_str)
    path_arr2 = []
    xy_counter = 0
    prev = 0
    for element in path_arr:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            is_num = False
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy="x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                path_arr2.append(float(prev))
        xy_counter += 1
    return min(path_arr2)


def get_max_x(path_str):
    path_arr = string_to_list(path_str)
    path_arr2 = []
    xy_counter = 0
    prev = 0
    for element in path_arr:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            is_num = False
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy="x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                path_arr2.append(float(prev))
        xy_counter += 1
    return max(path_arr2)


def get_min_y(path_str):
    path_arr = string_to_list(path_str)
    path_arr2 = []
    xy_counter = 0
    prev = 0
    for element in path_arr:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            is_num = False
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy="x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                path_arr2.append(float(element))
        xy_counter += 1
    return min(path_arr2)


def get_max_y(path_str):
    path_arr = string_to_list(path_str)
    path_arr2 = []
    xy_counter = 0
    prev = 0
    for element in path_arr:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            is_num = False
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy="x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                path_arr2.append(float(element))
        xy_counter += 1
    return max(path_arr2)


def get_destination_x(image):
    total_min, total_max = (None, None)
    for element in image.iter():
        if element.tag == '{http://www.w3.org/2000/svg}path':
            curr_min, curr_max = get_min_x(element.attrib['d']), get_max_x(element.attrib['d'])
            if total_min is None or curr_min < total_min:
                total_min = curr_min
            if total_max is None or curr_max < total_max:
                total_max = curr_max
    return (total_min + total_max)/2, total_max-total_min


def transform(rect, destination):
    rect_coords = [float(i) for i in rect.split()]
    x_coords = [rect_coords[i] for i in range(len(rect_coords)) if i % 2 == 0]
    return ' '.join([str(0), str(0),
                     str(max(x_coords)-min(x_coords)), str(destination.z)])


def change_parameters(element, transformed_rect, z, coef_y):
    orig_width, orig_height = [float(i) for i in element.attrib['viewBox'].split()[2:]]
    pr_width, pr_height = [float(i) for i in transformed_rect.split()[2:]]
    width = float(element.attrib['width'][:-2])
    element.attrib['height'] = str(z) + 'px'
    element.attrib['width'] = str(int(width*pr_width/orig_width)*coef_y) + 'px'


def scale_point(x, y, path, size_x=600, size_y=600):
    x_new = (x-path[0])*size_x/(path[2]-path[0])
    y_new = size_y - (y-path[1])*size_y/(path[3]-path[1])
    return Point(x_new, y_new)


def transform_path(path_str, old_path, size_x, size_y):
    path = [float(i) for i in old_path.split()]
    path_arr = string_to_list(path_str)
    path_arr1 = []
    path_arr2 = []
    xy_counter = 0
    prev = 0
    skip_steps = 7
    skip = 7
    for i in range(len(path_arr)):
        if re.match(r'a', str(path_arr[i]), flags=re.IGNORECASE):
            skip = 0
            path_arr1.extend(arc_to_bezier(path_arr[i-2], path_arr[i-1], path_arr[i+6], path_arr[i+7], path_arr[i+1],
                                           path_arr[i+2], path_arr[i+3], path_arr[i+4], path_arr[i+5]))
        elif skip < skip_steps:
            skip += 1
        else:
            path_arr1.append(path_arr[i])
    for element in path_arr1:
        if re.match(r'[mzlhvcsqta]', str(element), flags=re.IGNORECASE):
            xy_counter = -1
            is_num = False
            path_arr2.append(element)
        else:
            is_num = True
            element = float(element)
        if xy_counter % 2 == 0:
            xy = "x"
            prev = element
        else:
            xy = "y"
        if is_num:
            if xy == "y":
                point = scale_point(float(prev), element, path, size_x, size_y)
                path_arr2.append(point.x)
                path_arr2.append(point.y)
        xy_counter += 1
    path_str = list_to_string(path_arr2)
    return path_str


def vectorAngle(ux, uy, vx, vy):
    sign = -1 if ux * vy - uy * vx < 0 else 1
    umag = math.sqrt(ux * ux + uy * uy)
    vmag = math.sqrt(ux * ux + uy * uy)
    dot = ux * vx + uy * vy
    div = dot / (umag * vmag)

    if div > 1:
        div = 1

    if div < -1:
        div = -1
    return sign * math.acos(div)


def get_arc_center(px, py, cx, cy, rx, ry, large_flag, sweep, sinphi, cosphi, pxp, pyp):
    rxsq = math.pow(rx, 2)
    rysq = math.pow(ry, 2)
    pxpsq = math.pow(pxp, 2)
    pypsq = math.pow(pyp, 2)

    radicant = (rxsq * rysq) - (rxsq * pypsq) - (rysq * pxpsq)

    if radicant < 0:
        radicant = 0

    radicant /= (rxsq * pypsq) + (rysq * pxpsq)
    radicant = math.sqrt(radicant) * (-1 if large_flag ==  sweep else 1)

    centerxp = radicant * rx / ry * pyp
    centeryp = radicant * -ry / rx * pxp

    centerx = cosphi * centerxp - sinphi * centeryp + (px + cx) / 2
    centery = sinphi * centerxp + cosphi * centeryp + (py + cy) / 2

    vx1 = (pxp - centerxp) / rx
    vy1 = (pyp - centeryp) / ry
    vx2 = (-pxp - centerxp) / rx
    vy2 = (-pyp - centeryp) / ry

    ang1 = vectorAngle(1, 0, vx1, vy1)
    ang2 = vectorAngle(vx1, vy1, vx2, vy2)

    if sweep == 0 and ang2 > 0:
        ang2 -= TAU

    if sweep == 1 and ang2 < 0:
        ang2 += TAU

    return [centerx, centery, ang1, ang2]


def approxUnitArc(ang1, ang2):
    a = 4 / 3 * math.tan(ang2 / 4)
    x1 = math.cos(ang1)
    y1 = math.sin(ang1)
    x2 = math.cos(ang1 + ang2)
    y2 = math.sin(ang1 + ang2)
    return [Point(x1 - y1 * a,  y1 + x1 * a), Point(x2 + y2 * a, y2 - x2 * a), Point(x2, y2)]


def map_to_ellipse(point, rx, ry, cosphi, sinphi, centerx, centery):
    point.x *= rx
    point.y *= ry
    xp = cosphi * point.x - sinphi * point.y
    yp = sinphi * point.x + cosphi * point.y

    return Point(xp + centerx,yp + centery)


def arc_to_bezier(px, py, cx, cy, rx, ry, rotate_axis, large_flag, sweep):
    curves = []
    bezier_curves = []
    sinphi = math.sin(rotate_axis * TAU / 360)
    cosphi = math.cos(rotate_axis * TAU / 360)
    pxp = cosphi * (px - cx) / 2 + sinphi * (py - cy) / 2
    pyp = -sinphi * (px - cx) / 2 + cosphi * (py - cy) / 2
    rx = abs(rx)
    ry = abs(ry)
    l = (math.pow(pxp, 2) / math.pow(rx, 2)) + (math.pow(pyp, 2) / math.pow(ry, 2))
    if l > 1:
        rx *= math.sqrt(l)
        ry *= math.sqrt(l)
    centerx, centery, ang1, ang2 = get_arc_center(px, py, cx, cy, rx, ry,
                                                  large_flag, sweep, sinphi, cosphi, pxp, pyp)
    segments = max(math.ceil(abs(ang2) / (TAU / 4)), 1)
    ang2 /= segments
    for i in range(segments):
        curves.append(approxUnitArc(ang1, ang2))
        ang1 += ang2
    for curve in curves:
        p0 = map_to_ellipse(curve[0],  rx, ry, cosphi, sinphi, centerx, centery)
        p1 = map_to_ellipse(curve[1], rx, ry, cosphi, sinphi, centerx, centery)
        p2 = map_to_ellipse(curve[2], rx, ry, cosphi, sinphi, centerx, centery)

        bezier_curves.extend(['C', p0.x, p0.y, p1.x, p1.y, p2.x, p2.y])
    return bezier_curves


def get_height_width(vb, path_str):
    min_x, max_x, min_y, max_y = get_min_x(path_str), get_max_x(path_str), get_min_y(path_str), get_max_y(path_str)
    height = min_y+max_y
    width = max_x-min_x
    return width, height, min_y


def transform_image(fin, size_x=600, size_y=600, height=100, shadow_to_template=150, template_to_light=100,
                    coef_x=2.8345340734616746, coef_y=2.8345340734616746):
    f = etree.parse(fin)
    for element in f.iter():
        if element.tag == '{http://www.w3.org/2000/svg}svg':
            old_path = element.attrib['viewBox']
    paths, attrs = svg2paths(fin)
    wsvg(paths, attributes=attrs, filename=fin.split('.')[0]+'_new'+'.svg')
    f = etree.parse(fin.split('.')[0]+'_new'+'.svg')
    for element in f.iter():
        if element.tag == '{http://www.w3.org/2000/svg}svg':
            old_path = old_path
            element.attrib['viewBox'] = "0 0 %f %f" % (size_x, size_y)
            element.attrib['height'] = "%ipx" % size_x
            element.attrib['width'] = "%ipx" % size_y
        if element.tag == '{http://www.w3.org/2000/svg}path':
            element.attrib['d'] = transform_path(element.attrib['d'], old_path, size_x, size_y)
    f.write(fin.split('.')[0]+'_new'+'.svg')
    f = etree.parse(fin.split('.')[0] + '_new' + '.svg')
    for element in f.iter():
        if element.tag == '{http://www.w3.org/2000/svg}svg':
            vb_path = get_vb_path(element.attrib['viewBox'])
            destination = Point(size_x/2, template_to_light+shadow_to_template, height)
            print(destination.__dict__)
            new_vb = distort_path(vb_path, shadow_to_template, destination)
            min_x, max_x, min_y, max_y = retrieve_vb_from_rect(new_vb)
            transformed_rect = transform(new_vb, destination)
            change_parameters(element, transformed_rect, destination.z, coef_y)
            element.attrib['viewBox'] = transformed_rect
            rect = [int(float(i)*coef_x) for i in transformed_rect.split()]
    path_str = ''
    for element in f.iter():
        if element.tag == '{http://www.w3.org/2000/svg}path':
            element.attrib['d'] = distort_path(element.attrib['d'], shadow_to_template, destination, min_x, destination.z,
                                               coef_x=coef_x, coef_y=coef_y)
            path_str += ' ' + element.attrib['d']
    width, new_height, min_y = get_height_width(rect, path_str)
    for element in f.iter():
        if element.tag == '{http://www.w3.org/2000/svg}svg':
            element.attrib['viewBox'] = ' '.join(element.attrib['viewBox'].split()[:-2] + [str(width), str(new_height)])
            element.attrib['height'] = element.attrib['viewBox'].split()[3]+'px'
            element.attrib['width'] = element.attrib['viewBox'].split()[2] + 'px'
    print(new_height)
    filename = '.'.join(fin.split('.')[:-1]) + '_height_%.2fmm.svg' % ((rect[3] - new_height + min_y)/coef_y)
    f.write(filename)
    os.remove(fin.split('.')[0]+'_new'+'.svg')
    return filename

if __name__ == '__main__':
    print(transform_image('/home/alex/Downloads/figure.svg'))

